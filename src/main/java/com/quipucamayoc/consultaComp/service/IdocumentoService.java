package com.quipucamayoc.consultaComp.service;

import javax.servlet.http.HttpServletResponse;

import com.quipucamayoc.consultaComp.domain.*;

public interface IdocumentoService {
	String getNombreArchivo(formModel formulario);
	String getXML(formModel formulario);
	void getPDF(HttpServletResponse response,formModel formulario);
	boolean validarCatpcha(String recaptcha);
}