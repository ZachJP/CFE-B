package com.quipucamayoc.consultaComp.service.impl;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import com.quipucamayoc.consultaComp.domain.formModel;
import com.quipucamayoc.consultaComp.domain.recaptchaModel;
import com.quipucamayoc.consultaComp.service.IdocumentoService;
import com.quipucamayoc.consultaComp.service.ItipoCompService;
import com.quipucamayoc.consultaComp.util.Constantes;
import com.quipucamayoc.consultaComp.util.FTPManager;
import com.quipucamayoc.consultaComp.util.ReportDownloader;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class documentoServiceImpl implements IdocumentoService{
    // private static final Logger LOGGER =Logger.getLogger(documentoServiceImpl.class);
    
    @Autowired
	private ReportDownloader reportDownloader;

	@Autowired
	private ServletContext context;

	private FTPManager ftpManager=new FTPManager();

	private static final Logger LOGGER =Logger.getLogger(documentoServiceImpl.class);
	private SqlSessionFactory sqlSessionFactory;
	
	public ServletContext getContext() {
		return context;
	}

	public void setContext(ServletContext context) {
		this.context = context;
	}
	
	public documentoServiceImpl(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory=sqlSessionFactory;
    }
	@Override
	public void getPDF(HttpServletResponse response, formModel formulario) {
		System.out.println("formulario: "+formulario.getResponseRecaptcha());
		String mensaje="";
	 	if (validarCatpcha(formulario.getResponseRecaptcha())) {
			String valor = context.getRealPath("/reportes") + "\\";
			Map<String, Object> params = new HashMap<>();
			params.put("establecimiento", formulario.getEstablecimiento());
			params.put("serie", formulario.getNroComp());
			params.put("SUBREPORT_DIR", valor);
			String rutaLogo = context.getRealPath(Constantes.PATH_LOGO);
			String rutaLogoQuipu = context.getRealPath(Constantes.PATH_LOGO_QUIPU);
			String rutaLogoAnulado = context.getRealPath(Constantes.PATH_LOGO_ANULADO);
			params.put(Constantes.LOGO, rutaLogo);
			params.put(Constantes.LOGO_QUIPU, rutaLogoQuipu);
			params.put(Constantes.LOGO_ANULADO, rutaLogoAnulado);
			if (formulario.getTipoComp() == 1) {
				reportDownloader.downloadPDF(response, context.getRealPath("/reportes/boleta.jrxml"),
						"cxc_CP-20148092282-03-B" + formulario.getEstablecimiento() + "-" + formulario.getNroComp() + Constantes.EXTENSION_PDF, params,false, false);
			} else {
				if (formulario.getTipoComp() == 2) {
					reportDownloader.downloadPDF(response, context.getRealPath("/reportes/factura.jrxml"),
							"cxc_CP-20148092282-01-F" + formulario.getEstablecimiento() + "-" + formulario.getNroComp() + Constantes.EXTENSION_PDF, params,false, false);
				} else if (formulario.getTipoComp() == 4) {
					params.put("tipo", formulario.getTipoComp());
					reportDownloader.downloadPDF(response, context.getRealPath("/reportes/notaCredito.jrxml"),
							"cxc_CP-20148092282-07-F" + formulario.getEstablecimiento() + "-" + formulario.getNroComp() + Constantes.EXTENSION_PDF, params,false, false);
				} else if (formulario.getTipoComp() == 5) {
					params.put("tipo", formulario.getTipoComp());
					reportDownloader.downloadPDF(response, context.getRealPath("/reportes/notaCredito.jrxml"),
							"cxc_CP-20148092282-07-B" + formulario.getEstablecimiento() + "-" + formulario.getNroComp() + Constantes.EXTENSION_PDF, params,false, false);
				}
			}
			mensaje="descargando PDF";
			System.out.println("saliendo del metodo");
		} else {
			mensaje="error en el recaptcha";
		} 
	}

	@Override
	public boolean validarCatpcha(String recaptcha) {
		RestTemplate restTemplate = new RestTemplate();
		URI verifyUri = URI.create(String.format(
			Constantes.URL_VERIFY_RECATPCHA,
			Constantes.KEY_PRIVATE_RECATPCHA, recaptcha, ""));
		  recaptchaModel response = restTemplate.getForObject(verifyUri, recaptchaModel.class);
		return response.getSuccess();
	}

	@Override
	public String getXML(formModel formulario) {
		String rpta_ftp="";
		tipoCompServiceImpl tImpl=new tipoCompServiceImpl(sqlSessionFactory);
		System.out.println ("Existe: "+ tImpl.getComp(formulario));
		if(tImpl.getComp(formulario)){
			String nombre_archivo=getNombreArchivo(formulario);
			System.out.println("Nombre Archivo: "+nombre_archivo);
			rpta_ftp = FTPManager.getFTP("./SFE/DES/FIRMA/"+nombre_archivo);
			return rpta_ftp;
		}else{
			rpta_ftp="no encontrado";
		}
		// if(tImpl.getComp(formulario)){
		// 	ftpManager.getFTP(filename);
		// }
		//comprobar existencia
		//if(existe)
		//	accesoFTP
		return rpta_ftp;
	}

	@Override
	public String getNombreArchivo(formModel formulario) {
		String archivo="";
        switch(formulario.getTipoComp()){
            case 1: archivo=Constantes.RUC_SANMARCOS+Constantes.FORMATO_BOLETA+formulario.getEstablecimiento()+"-"+formulario.getNroComp()+Constantes.EXTENSION_XML;
            break;
            case 2: archivo=Constantes.RUC_SANMARCOS+Constantes.FORMATO_FACTURA+formulario.getEstablecimiento()+"-"+formulario.getNroComp()+Constantes.EXTENSION_XML;
            break;
            case 4: archivo=Constantes.RUC_SANMARCOS+Constantes.FORMATO_NOTAC_BOLETA+formulario.getEstablecimiento()+"-"+formulario.getNroComp()+Constantes.EXTENSION_XML;
            break;
            case 5: archivo=Constantes.RUC_SANMARCOS+Constantes.FORMATO_NOTAC_FACTURA+formulario.getEstablecimiento()+"-"+formulario.getNroComp()+Constantes.EXTENSION_XML;
            break;
        }

		return archivo;
	}

}