package com.quipucamayoc.consultaComp.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.quipucamayoc.consultaComp.domain.comprobanteModel;
import com.quipucamayoc.consultaComp.domain.formModel;
import com.quipucamayoc.consultaComp.domain.tipoComp;
import com.quipucamayoc.consultaComp.service.ItipoCompService;
import com.quipucamayoc.consultaComp.util.*;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class tipoCompServiceImpl implements ItipoCompService{
    /**
	 *
	 */
	private static final Logger LOGGER =Logger.getLogger(tipoCompServiceImpl.class);
    private SqlSessionFactory sqlSessionFactory;

    public tipoCompServiceImpl(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory=sqlSessionFactory;
    }
	@Override
	public List<tipoComp> getTypes() {
        List<tipoComp> typesComp=new ArrayList<>();

        SqlSession session = sqlSessionFactory.openSession();

        try {
            if(session!=null){
                typesComp=session.selectList("com.quipucamayoc.mybatis.mapper.tipoCompMapper.getTiposComprobantes");
            }
        } catch (Exception e) {
            LOGGER.error("com.quipucamayoc.mybatis.mapper.tipoCompMapper.getTiposComprobantes: "+e);
        }finally{
            LOGGER.info("cerrar session tipo comprobantes");
            session.close();
        }
		return typesComp;
	}
	@Override
	public boolean getComp(formModel formulario) {
        boolean existe_comp=false;
        SqlSession session=sqlSessionFactory.openSession();
        try{
            comprobanteModel comp=new comprobanteModel();
            if(session!=null){
                System.out.println("formulario "+formulario);
                comp=session.selectOne("com.quipucamayoc.mybatis.mapper.CompMapper.getComprobante",formulario);
                if(comp!=null){
                    existe_comp=true;
                }
            }
        }catch(Exception e){
            LOGGER.error("com.quipucamayoc.mybatis.mapper.CompMapper.getComprobante"+e);
        }finally{
            LOGGER.info("cerrar session obtener comprobante");
            session.close();
        }
		return existe_comp;
	}

}