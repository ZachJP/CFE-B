package com.quipucamayoc.consultaComp.service;

import java.util.List;

import com.quipucamayoc.consultaComp.domain.*;

public interface ItipoCompService {
	List <tipoComp> getTypes();
	boolean getComp(formModel formulario);
}