package com.quipucamayoc.consultaComp.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.codec.binary.Base64;

public class FTPManager {
    
    private static final int BUFFER_SIZE = 4096;
    private static final String URL_FTP = "ftp://userTesoreria:us3rT3s0r3r14Pr0$@172.16.156.183:21";
    
    public Boolean saveFTP(String base64string, String filename){
        String ftpUrl = URL_FTP;
        byte[] decoded = DatatypeConverter.parseBase64Binary(base64string);
        ByteArrayInputStream stream = new ByteArrayInputStream(decoded);

        try {
            URL url = new URL(ftpUrl+"/../portalPostulacion/"+filename);
            URLConnection conn = url.openConnection();
            OutputStream outputStream = conn.getOutputStream();

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1 ;
            while ((bytesRead = stream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            stream.close();
            outputStream.close();

            System.out.println("File uploaded");
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public static String getFTP(String path_file){
        String salida=null;
        FTPClient ftpClient = new FTPClient();
       
        try{
            ftpClient.connect(Constantes.IP_FTP,Constantes.PORT_FTP);
            ftpClient.login(Constantes.USER_FTP, Constantes.USER_PASSWRD);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            System.out.println("1");
            InputStream inputStream = new BufferedInputStream(ftpClient.retrieveFileStream(path_file));
            System.out.println("2");
            // inputStream.close();
            System.out.println(" "+IOUtils.toByteArray(inputStream));
            salida = Base64.encodeBase64String(IOUtils.toByteArray(inputStream));
            System.out.println("3");
            if(ftpClient.completePendingCommand()){
                return salida;
            }
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
                if (ftpClient.isConnected()) {
                    try {
                        ftpClient.logout();
                        ftpClient.disconnect();
                    } catch (IOException ex) {
                        Logger.getLogger(FTPManager.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
        }
        return null;
    }
}