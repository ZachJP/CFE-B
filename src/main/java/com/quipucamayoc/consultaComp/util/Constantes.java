package com.quipucamayoc.consultaComp.util;
public class Constantes{
    public static final String RUC_SANMARCOS="20148092282";
    public static final String EXTENSION_XML=".xml";
    public static final String FORMATO_BOLETA="-03-B";
    public static final String FORMATO_FACTURA="-01-F";
    public static final String FORMATO_NOTAC_BOLETA="-07-B";
    public static final String FORMATO_NOTAC_FACTURA="-07-F";
    public static final String URL_VERIFY_RECATPCHA="https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s";
    public static final String KEY_PRIVATE_RECATPCHA="6LdtQkEUAAAAAMQKNW7lft3MogDHBhUEPLDY8-wo";
    public static final String EXTENSION_PDF = ".pdf";
	public static final String REPORT_LOCALE = "REPORT_LOCALE";
	public static final String LOGO_QUIPU = "logo_quipu";
	public static final String LOGO_ANULADO = "anulado";
	public static final String LOGO = "logo";
	public static final String PATH_LOGO = "/reportes/EscudoUNMSM.png";
	public static final String PATH_LOGO_QUIPU = "/reportes/logo_quipu.png";
    public static final String PATH_LOGO_ANULADO = "/reportes/anulado.png";
    public static final String USER_FTP="userTesoreria";
    public static final String USER_PASSWRD="us3rT3s0r3r14Pr0$";
    public static final String IP_FTP="172.16.156.183";
    public static final int PORT_FTP=21;
}