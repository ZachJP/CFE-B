package com.quipucamayoc.consultaComp.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import oracle.jdbc.pool.OracleDataSource;

public class MyDataSourceFactory{
	ClassLoader loader = Thread.currentThread().getContextClassLoader();
    public DataSource getOracleDataSource(){
        Properties props = new Properties();
		InputStream fis = null;
		OracleDataSource oracleDS = null;
		try {
			fis = getClass().getClassLoader().getResourceAsStream("application.properties");
			props.load(fis);
			oracleDS = new OracleDataSource();
			oracleDS.setURL(props.getProperty("spring.datasource.url"));
			oracleDS.setUser(props.getProperty("spring.datasource.username"));
			oracleDS.setPassword(props.getProperty("spring.datasource.password"));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return oracleDS;
    }
}