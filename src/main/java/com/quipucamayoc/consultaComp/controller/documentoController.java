package com.quipucamayoc.consultaComp.controller;

import javax.servlet.http.HttpServletResponse;

import com.quipucamayoc.consultaComp.domain.formModel;
import com.quipucamayoc.consultaComp.service.IdocumentoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cfe/api/documento")
public class documentoController {

    @Autowired
	private IdocumentoService idocumentoService;
    
    @RequestMapping(method = RequestMethod.POST, value = "/downloadPDF")
	public void generarDocumentoPDF(HttpServletResponse response, @RequestBody formModel formulario) {
        idocumentoService.getPDF(response,formulario);
    }
    
    @RequestMapping(method=RequestMethod.POST, produces = "application/json", value="/downloadXML")
    public String generarDocumentoXML(@RequestBody formModel formulario){
        return idocumentoService.getXML(formulario);
    }
}