package com.quipucamayoc.consultaComp.controller;

import java.util.List;

import com.quipucamayoc.consultaComp.domain.tipoComp;
import com.quipucamayoc.consultaComp.service.ItipoCompService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cfe/api")
public class tipoCompController {
    
    @Autowired
	private ItipoCompService itipoCompService;
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json", value = "/tiposComp")
	public List<tipoComp> listarTiposComp() {
		System.out.println(itipoCompService.getTypes());
		return itipoCompService.getTypes();
	}
}
