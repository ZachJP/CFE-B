package com.quipucamayoc.consultaComp.domain;

// import java.util.List;

public class recaptchaModel{
    private boolean success;
    private String challengue_ts;
    private String hostname;

    public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
    }
    
    public String getChallengue_ts() {
		return challengue_ts;
	}

	public void setChallengue_ts(String challengue_ts) {
		this.challengue_ts = challengue_ts;
    }
    
    public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
}