package com.quipucamayoc.consultaComp.domain;

public class formModel {
    private Integer tipoComp;
    private String nroComp;
    private String fechaEmision;
    private String nroDoc;
    private String establecimiento;
    private String responseRecaptcha;

    public Integer getTipoComp() {
		return tipoComp;
	}

	public void setTipoComp(Integer tipoComp) {
		this.tipoComp = tipoComp;
	}

    public String getEstablecimiento(){
        return establecimiento;
    }

    public void setEstablecimiento(String establecimiento){
        this.establecimiento=establecimiento;
    }

    public String getNroComp() {
		return nroComp;
	}

	public void setNroComp(String nroComp) {
		this.nroComp = nroComp;
    }
    
    public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
    }

    public String getNroDoc() {
		return nroDoc;
	}

	public void setNroDoc(String nroDoc) {
		this.nroDoc = nroDoc;
    }

    public String getResponseRecaptcha() {
		return responseRecaptcha;
	}

	public void setResponseRecaptcha(String responseRecaptcha) {
		this.responseRecaptcha = responseRecaptcha;
    }
}