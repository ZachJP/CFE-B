package com.quipucamayoc.consultaComp.domain;
import java.sql.Date;

public class comprobanteModel{
    private int anio;
    private int mes;
    private int tipo;
    private String cod_estab;
    private String serie;
    private String fecha_emision;
    private String doc_iden;
    private String nombre_cliente;

    public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public String getCod_estab() {
		return cod_estab;
	}
	public void setCod_estab(String cod_estab) {
		this.cod_estab = cod_estab;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getFecha_emision() {
		return fecha_emision;
	}
	public void setFecha_emision(String fecha_emision) {
		this.fecha_emision = fecha_emision;
	}
	public String getDoc_iden() {
		return doc_iden;
	}
	public void setDoc_iden(String doc_iden) {
		this.doc_iden = doc_iden;
	}
	public String getNombre_cliente() {
		return nombre_cliente;
	}
	public void setNombre_cliente(String nombre_cliente) {
		this.nombre_cliente = nombre_cliente;
	}
}