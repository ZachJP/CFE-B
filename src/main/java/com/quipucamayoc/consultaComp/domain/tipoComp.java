package com.quipucamayoc.consultaComp.domain;

public class tipoComp {

	private Integer id_tipo_cpago;

	private String nombre;

	private String desc;

	private Integer est;

	private String sunat_eq;

	private String inicial;
	
	
	public Integer getId_tipo_cpago() {
		return id_tipo_cpago;
	}


	public void setId_tipo_cpago(Integer id_tipo_cpago) {
		this.id_tipo_cpago = id_tipo_cpago;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}


	public Integer getEst() {
		return est;
	}


	public void setEst(Integer est) {
		this.est = est;
	}


	public String getSunat_eq() {
		return sunat_eq;
	}


	public void setSunat_eq(String sunat_eq) {
		this.sunat_eq = sunat_eq;
	}


	public String getInicial() {
		return inicial;
	}


	public void setInicial(String inicial) {
		this.inicial = inicial;
	}


	@Override
	public String toString() {
		return getId_tipo_cpago() + "," + getNombre() + "," + getDesc() + "," + getSunat_eq();
	}

}
